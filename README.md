# Frontend Dev Test 2 #
1. Clone this repository
1. Create a branch: `lastname-firstname`
1. Run `npm run start`
1. Read the instructions page thoroughly
1. Work no more than a couple hours (we know your time is valuable)
1. Create a pull request to this repository

**_Rogue Frontend - Move Fast and Break Things_**